resource "helm_release" "kyverno" {
  name = "kyverno"

  repository = "https://kyverno.github.io/kyverno/"
  chart      = "kyverno"
  version    = "2.7.2"

  namespace        = "kyverno"
  create_namespace = true

  set {
    name  = "replicaCount"
    value = 1
  }
}
