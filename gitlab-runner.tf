# Installs gitlab runner, so we can run CI tasks on k8s
resource "helm_release" "gitlab_runner" {
  name = "gitlab-runner"

  repository = "https://charts.gitlab.io"
  chart      = "gitlab-runner"
  version    = "0.71.0"

  namespace = "gitlab-runner"

  set {
    name  = "gitlabUrl"
    value = "https://git.tardisproject.uk"
  }

  set {
    # The name of the secret containing runner-token and runner-registration-token
    name  = "runners.secret"
    value = "gitlab-runner-secret"
  }

  set {
    # Specifies whether a Role and RoleBinding should be created
    name  = "rbac.create"
    value = true
  }

  set {
    # Using an tad different version of the default runner configuration from https://docs.gitlab.com/charts/charts/gitlab/gitlab-runner/#default-runner-configuration
    name  = "runners.config"
    value = <<EOT
    [[runners]]
      [runners.kubernetes]
        image = "ubuntu:22.04"
      [runners.cache]
        Type = "s3"
        Path = "runner"
        Shared = true
        [runners.cache.s3]
          ServerAddress = "minio.internal.tardisproject.uk:9000"
          BucketName = "gitlab-runner-cache"
          BucketLocation = "optional"
          Insecure = true
          AuthenticationType = "access-key"
    EOT
  }

  set {
    name  = "runners.cache.secretName"
    value = "s3access"
  }
}
