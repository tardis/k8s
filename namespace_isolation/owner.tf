# Create an owners clusterrole. This defines what each user can do in their own namespace
resource "kubernetes_cluster_role" "owner" {
  metadata {
    name = "owner"
  }

  rule {
    verbs      = ["get", "list", "watch"]
    api_groups = [""]
    resources  = ["pods/attach", "pods/exec", "pods/portforward", "pods/proxy", "secrets", "services/proxy"]
  }

  rule {
    verbs      = ["impersonate"]
    api_groups = [""]
    resources  = ["serviceaccounts"]
  }

  rule {
    verbs      = ["create", "delete", "deletecollection", "patch", "update"]
    api_groups = [""]
    resources  = ["pods", "pods/attach", "pods/exec", "pods/portforward", "pods/proxy"]
  }

  rule {
    verbs      = ["create"]
    api_groups = [""]
    resources  = ["pods/eviction"]
  }

  rule {
    verbs      = ["create", "delete", "deletecollection", "patch", "update"]
    api_groups = [""]
    resources  = ["configmaps", "events", "persistentvolumeclaims", "replicationcontrollers", "replicationcontrollers/scale", "secrets", "serviceaccounts", "services", "services/proxy"]
  }

  rule {
    verbs      = ["create"]
    api_groups = [""]
    resources  = ["serviceaccounts/token"]
  }

  rule {
    verbs      = ["create", "delete", "deletecollection", "patch", "update"]
    api_groups = ["apps"]
    resources  = ["daemonsets", "deployments", "deployments/rollback", "deployments/scale", "replicasets", "replicasets/scale", "statefulsets", "statefulsets/scale"]
  }

  rule {
    verbs      = ["create", "delete", "deletecollection", "patch", "update"]
    api_groups = ["autoscaling"]
    resources  = ["horizontalpodautoscalers"]
  }

  rule {
    verbs      = ["create", "delete", "deletecollection", "patch", "update"]
    api_groups = ["batch"]
    resources  = ["cronjobs", "jobs"]
  }

  rule {
    verbs      = ["create", "delete", "deletecollection", "patch", "update"]
    api_groups = ["extensions"]
    resources  = ["daemonsets", "deployments", "deployments/rollback", "deployments/scale", "ingresses", "networkpolicies", "replicasets", "replicasets/scale", "replicationcontrollers/scale"]
  }

  rule {
    verbs      = ["create", "delete", "deletecollection", "patch", "update"]
    api_groups = ["policy"]
    resources  = ["poddisruptionbudgets"]
  }

  rule {
    verbs      = ["create", "delete", "deletecollection", "patch", "update"]
    api_groups = ["networking.k8s.io"]
    resources  = ["ingresses", "networkpolicies"]
  }

  rule {
    verbs      = ["create", "delete", "deletecollection", "get", "list", "patch", "update", "watch"]
    api_groups = ["coordination.k8s.io"]
    resources  = ["leases"]
  }

  rule {
    verbs      = ["get", "list", "watch"]
    api_groups = ["metrics.k8s.io"]
    resources  = ["pods", "nodes"]
  }

  rule {
    verbs      = ["get", "list", "watch"]
    api_groups = [""]
    resources  = ["configmaps", "endpoints", "persistentvolumeclaims", "persistentvolumeclaims/status", "pods", "replicationcontrollers", "replicationcontrollers/scale", "serviceaccounts", "services", "services/status"]
  }

  rule {
    verbs      = ["get", "list", "watch"]
    api_groups = [""]
    resources  = ["bindings", "events", "limitranges", "namespaces/status", "pods/log", "pods/status", "replicationcontrollers/status", "resourcequotas", "resourcequotas/status"]
  }

  rule {
    verbs      = ["get", "list", "watch"]
    api_groups = [""]
    resources  = ["namespaces"]
  }

  rule {
    verbs      = ["get", "list", "watch"]
    api_groups = ["discovery.k8s.io"]
    resources  = ["endpointslices"]
  }

  rule {
    verbs      = ["get", "list", "watch"]
    api_groups = ["apps"]
    resources  = ["controllerrevisions", "daemonsets", "daemonsets/status", "deployments", "deployments/scale", "deployments/status", "replicasets", "replicasets/scale", "replicasets/status", "statefulsets", "statefulsets/scale", "statefulsets/status"]
  }

  rule {
    verbs      = ["get", "list", "watch"]
    api_groups = ["autoscaling"]
    resources  = ["horizontalpodautoscalers", "horizontalpodautoscalers/status"]
  }

  rule {
    verbs      = ["get", "list", "watch"]
    api_groups = ["batch"]
    resources  = ["cronjobs", "cronjobs/status", "jobs", "jobs/status"]
  }

  rule {
    verbs      = ["get", "list", "watch"]
    api_groups = ["extensions"]
    resources  = ["daemonsets", "daemonsets/status", "deployments", "deployments/scale", "deployments/status", "ingresses", "ingresses/status", "networkpolicies", "replicasets", "replicasets/scale", "replicasets/status", "replicationcontrollers/scale"]
  }

  rule {
    verbs      = ["get", "list", "watch"]
    api_groups = ["policy"]
    resources  = ["poddisruptionbudgets", "poddisruptionbudgets/status"]
  }

  rule {
    verbs      = ["get", "list", "watch"]
    api_groups = ["networking.k8s.io"]
    resources  = ["ingresses", "ingresses/status", "networkpolicies"]
  }

  rule {
    verbs      = ["get", "list", "watch", "create", "delete", "patch", "update"]
    api_groups = ["rbac.authorization.k8s.io"]
    resources  = ["roles"]
  }

  rule {
    verbs      = ["get", "list", "watch", "create", "delete", "patch", "update"]
    api_groups = ["rbac.authorization.k8s.io"]
    resources  = ["rolebindings"]
  }
}
