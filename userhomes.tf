# Forward to userhomes daemon by default
# When someone makes a gitlab page, it gets added as a different rule by the tardis console
# Likewise for custom endpoints (or for kubernetes ingress routes)
# This means we get all 3 :)
resource "kubernetes_ingress_v1" "userhomes-default" {
  metadata {
    name      = "userhomes"
    namespace = "tardis"
    annotations = {
      "traefik.ingress.kubernetes.io/router.priority" : "2"
      "traefik.ingress.kubernetes.io/router.tls" : "true"
      "traefik.ingress.kubernetes.io/router.tls.certresolver"   = "letsencrypt"
      "traefik.ingress.kubernetes.io/router.tls.domains.0.main" = "*.tardis.ac"
    }
  }

  spec {
    rule {
      host = "*.tardis.ac"
      http {
        path {
          path = "/"

          backend {
            service {
              name = "userhomes"
              port {
                name = "http"
              }
            }
          }
        }
      }
    }
  }
}


# Access userhomes daemon from inside cluster
resource "kubernetes_service" "userhomes" {
  metadata {
    name      = "userhomes"
    namespace = "tardis"
  }
  spec {
    type = "ClusterIP"

    port {
      name        = "http"
      port        = 80
      target_port = 8181
    }
  }
}

resource "kubernetes_endpoints" "userhomes" {
  metadata {
    name      = "userhomes"
    namespace = "tardis"
  }
  subset {
    address {
      ip = "192.168.0.14"
    }
    port {
      name     = "http"
      port     = 8181
      protocol = "TCP"
    }
  }
}
