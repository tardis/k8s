# Sets up metallb, for assigning load balancer IPs

resource "helm_release" "metallb" {
  name = "metallb"

  repository = "https://metallb.github.io/metallb"
  chart      = "metallb"
  version    = "0.13.9"

  namespace        = "metallb"
  create_namespace = true
}

resource "kubernetes_manifest" "ipaddresspool_primary" {
  manifest = {
    "apiVersion" = "metallb.io/v1beta1"
    "kind"       = "IPAddressPool"
    "metadata" = {
      "name"      = "primary"
      "namespace" = "metallb"
    }
    "spec" = {
      "addresses" = [
        "10.0.0.2-10.0.3.255" # 10.0.0.0/22 minus an address for the router
      ]
    }
  }
}

resource "kubernetes_manifest" "l2advertisement" {
  manifest = {
    "apiVersion" = "metallb.io/v1beta1"
    "kind"       = "L2Advertisement"
    "metadata" = {
      "name"      = "l2advertise"
      "namespace" = "metallb"
    }
  }
}
