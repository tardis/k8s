# Set up a service account for sonic-screwdriver, allow it to manage namespaces, and create a secret containing a token for it
resource "kubernetes_service_account" "sonic-screwdriver-serviceaccount" {
  metadata {
    name      = "sonic-screwdriver"
    namespace = "tardis"
  }
}

resource "kubernetes_secret" "sonic-screwdriver-secret" {
  metadata {
    name      = "sonic-screwdrier-token"
    namespace = "tardis"
    annotations = {
      "kubernetes.io/service-account-name" = "sonic-screwdriver"
    }
  }
}

resource "kubernetes_cluster_role" "sonic-screwdriver-clusterrole" {
  metadata {
    name = "sonic-screwdriver"
  }
  rule {
    api_groups = [""]
    resources  = ["namespaces"]
    verbs = [
      "get",
      "list",
      "watch",
      "create",
      "delete",
      "deletecollection",
      "patch",
      "update"
    ]
  }
}

resource "kubernetes_cluster_role_binding" "sonic-screwdriver-clusterrolebinding" {
  metadata {
    name = "tardis:sonic-screwdriver"
  }
  role_ref {
    kind      = "ClusterRole"
    name      = "sonic-screwdriver"
    api_group = "rbac.authorization.k8s.io"
  }
  subject {
    kind      = "ServiceAccount"
    name      = "sonic-screwdriver"
    namespace = "tardis"
  }
}
