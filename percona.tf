# Sets up a percona mysql operator
# This is used for user databases

resource "helm_release" "percona_operator" {
  name = "pxc-operator"

  repository = "https://percona.github.io/percona-helm-charts/"
  chart      = "pxc-operator"
  version    = "1.12.1"

  namespace        = "percona"
  create_namespace = true
}

resource "helm_release" "percona_database" {
  name = "pxc-db"

  repository = "https://percona.github.io/percona-helm-charts/"
  chart      = "pxc-db"
  version    = "1.12.2"

  namespace        = "percona"
  create_namespace = true

  values = [
    <<EOF
finalizers:
  - delete-pxc-pods-in-order
  - delete-proxysql-pvc

nameOverride: "production"
fullnameOverride: "production"

updateStrategy: SmartUpdate
upgradeOptions:
  versionServiceEndpoint: https://check.percona.com
  apply: Recommended
  schedule: "0 4 * * *"

tls:
  issuerConf:
    name: selfsigned
    kind: ClusterIssuer
    group: cert-manager.io

pxc:
  readinessDelaySec: 15
  livenessDelaySec: 300
  size: 1
  resources:
    requests:
      memory: 2G
      cpu: 1
    limits: {}
  podDisruptionBudget:
    maxUnavailable: 1
  persistence:
    enabled: true
    storageClass: openebs-hostpath
    size: 20Gi
  certManager: true
  autoRecovery: true
  clusterSecretName: db-create-secrets

haproxy:
  enabled: true
  size: 1
  serviceType: LoadBalancer

proxysql:
  enabled: false

logcollector:
  enabled: false

pmm:
  enabled: false

backup:
  enabled: true
  pitr:
    enabled: false
  storages:
    fs-pvc:
      type: filesystem
      volume:
        persistentVolumeClaim:
          accessModes: ["ReadWriteOnce"]
          resources:
            requests:
              storage: 10Gi

  schedule:
    - name: "daily-backup"
      schedule: "0 0 * * *"
      keep: 5
      storageName: fs-pvc
EOF
  ]
}
