# Forward custom domains (so you can alias dns to tardis.ac), but don't terminate TLS as gitlab gets those certs.
resource "kubernetes_manifest" "ingressroutetcp_gitlab_pages_default_gitlab_pages" {
  manifest = {
    "apiVersion" = "traefik.io/v1alpha1"
    "kind"       = "IngressRouteTCP"
    "metadata" = {
      "name"      = "default-gitlab-pages"
      "namespace" = "tardis"
    }
    "spec" = {
      "entryPoints" = [
        "websecure",
      ]
      "routes" = [
        {
          "priority" = 1
          "match" = "!(HostSNI(`tardis.ac`) || HostSNIRegexp(`^[^\\.]*\\.tardis\\.ac$`))"
          "services" = [
            {
              "name" = "gitlab-pages"
              "port" = 443
            },
          ]
        },
      ]
      "tls" = {
        "passthrough" = true
      }
    }
  }
}

# Access gitlab pages daemon from inside cluster
resource "kubernetes_service" "gitlab-pages" {
  metadata {
    name      = "gitlab-pages"
    namespace = "tardis"
  }
  spec {
    type = "ClusterIP"

    port {
      name        = "http"
      port        = 80
      target_port = 8090
    }
    port {
      name        = "https"
      port        = 443
      target_port = 8443
    }
  }
}

resource "kubernetes_endpoints" "gitlab-pages" {
  metadata {
    name      = "gitlab-pages"
    namespace = "tardis"
  }
  subset {
    address {
      ip = "192.168.0.69"
    }
    port {
      name     = "http"
      port     = 8090
      protocol = "TCP"
    }
    port {
      name     = "https"
      port     = 9443
      protocol = "TCP"
    }
  }
}
