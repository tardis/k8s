# tardis-k8s

This contains some manifests used for our k8s cluster - it deals with things like namespace isolation, etc.

If you just want to use k8s, go [here](https://wiki.tardisproject.uk/howto:learn_k8s).

## Developing

We use [Terraform](https://developer.hashicorp.com/terraform/docs) with the kubernetes provider.

It assumes you have a context named `tardis-admin` in your kubernetes config, that logs in as a cluster admin. State is stored in kubernetes itself, so if you have this you should only need to do `terraform init`.

Use `terraform plan` to preview your changes, then `terraform apply` to apply them. We don't currently have any CI to auto apply them.

See the wiki article [here](https://wiki.tardisproject.uk/hosts:k8s:working-with-terraform).