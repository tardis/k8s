# Install cert-manager and configure letsencrypt + our internal CA
resource "helm_release" "cert-manager" {
  name = "cert-manager"

  repository = "https://charts.jetstack.io"
  chart      = "cert-manager"
  version    = "v1.16.3"

  namespace        = "cert-manager"
  create_namespace = true

  set {
    name  = "installCRDs"
    value = true
  }
}
resource "kubernetes_manifest" "clusterissuer_selfsigned" {
  manifest = {
    apiVersion = "cert-manager.io/v1"
    kind       = "ClusterIssuer"
    metadata = {
      name = "selfsigned"
    }
    spec = {
      selfSigned = {}
    }
  }
}

resource "kubernetes_manifest" "clusterissuer_letsencrypt" {
  manifest = {
    apiVersion = "cert-manager.io/v1"
    kind       = "ClusterIssuer"
    metadata = {
      name = "letsencrypt"
    }
    spec = {
      acme = {
        email  = "sysmans+k8s@tardisproject.uk"
        server = "https://acme-staging-v02.api.letsencrypt.org/directory"
        privateKeySecretRef = {
          name = "letsencrypt-issuer-account-key"
        }

        solvers = [
          {
            http01 = {
              ingress = {
                class = "traefik"
              }
            }
            selector = {
              dnsNames = ["tardis.ac", "*.tardis.ac", "k8s.tardisproject.uk"]
            }
          }
        ]
      }
    }
  }
}
